Rails.application.routes.draw do
  post "/get-offer", to: "offers#get_offers"
  post "/create-offer", to: "offers#create_offer"
  match '*a', to: 'application#render_404', via: [:get, :post, :put, :delete]
end
