class OffersController  < ApplicationController
	skip_before_action :verify_authenticity_token
	def get_offers
		begin
			Rails.logger.info "GET_OFFERS: params #{params.inspect}"
			if(params['offset'] != nil and params['limit'] != nil)
				offset = params['offset'].to_i
				limit = params['limit'].to_i
				# offers = Offer.find_by_limit(offset,limit)
				offers = Offer.skip(offset).limit(limit)
				render status: 200, json: offers
				return
			else
				logger.error "GET_OFFERS: params not found"
				render status: 404, json: {:text => "params not found"}	
				return	
			end	
		rescue Exception => e
			logger.error "GET_OFFERS: Exception => #{e.inspect} #{e.backtrace}"
			render status: 404, json: {:text => "failed"}
			return
		end
	end

	def create_offer
		begin
			logger.info "CREATE_OFFERS: params #{params.inspect}"
			offer_new = {}.with_indifferent_access
			offer_new[:city] = params["city"]
			offer_new[:shop_id] = params["shop_id"]
			offer_new[:product_id] = params["product_id"]
			offer_new[:mall_id] = params["mall_id"]
			offer_new[:details] = params["details"]
			offer_new[:offer_ends] = params["offer_ends"]
			offer_new[:offer_starts] = params["offer_starts"]
			logger.info "CREATE_OFFERS: offer #{offer_new}"
			offer = Offer.new(offer_new)
			if(offer.save!)
				logger.info "CREATE_OFFERS: created successful"
				render status: 200, json: {:text => "saved successful"}
				return
			else
				logger.info "CREATE_OFFERS: failed to save"
				render status: 404, json: {:text => "could not save"}
				return
			end
		rescue Exception => e
			logger.info "CREATE_OFFERS: Exception #{e.inspect} #{e.backtrace}"
			render status: 404, json: {:text => "Exception occured"}
			return
		end
	end

end