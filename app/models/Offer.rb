require 'mongoid'
class Offer
    include Mongoid::Document
    include Mongoid::Timestamps
    store_in collection: "offers"
    field :mall_id, type: Integer
    field :shop_id, type: Integer
    field :product_id, type: String
    field :details, type: String
    field :city, type: String
    field :created_at, type: DateTime, default: ->{ Time.now }
    field :updated_at, type: DateTime, default: ->{ Time.now }
    field :offer_ends, type: DateTime
    field :offer_starts, type: DateTime, default: ->{ Time.now }
    field :views, type: Integer, default: ->{0}
    field :likes, type: Integer, default: ->{0}
    index ({city: 1})
    index ({mall_id: 1, shop_id: 1, product_id: 1})
    index ({offer_starts: 1, offer_ends: 1})
    
end